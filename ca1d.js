"use strict";

function allocate(L,T) {
    let cells = new Array(T);
    let t= 0;
    while( t < T ) {
        cells[t] = new Array(L); 
        t+=1;
    }
    return cells;
}

function setup(L,T,init) {
    let c = allocate(L,T);
    const t = 0;
    let x = 0;
    while( x < L) {
        c[t][x] = init(x);
        x += 1;
    }
    return {
        current: c,
        L: L,
	T: T,
    };
}

function update( t, cells, rule ) {
    const N = cells.L;
    const next = cells.current[t];
    const last = cells.current[t-1];
    next[0] = rule( last[N-1], last[0], last[1] );
    let x = 1;
    while( x < N ) {
	next[x] = rule( last[x-1],last[x],last[x+1] );
	x += 1;
    }
    next[N-1] = rule( last[N-2], last[N-1], last[0] );
}

function drawCells(context,cells,T) {
    context.clearRect ( 0, 0, context.width, context.height );
    context.save();
    let t = 0;
    while( t < T ) {
        let x = 0;
        while( x < cells.L) {
            let col = "black";
	    if( cells.current[t][x] === 1 ) {
		col = "white";
	    }
            context.fillStyle = col;
            context.fillRect( x-1, t-1, 1, 1);
            x += 1;
        }
        t += 1;
    }
    context.restore();
}


function start( context,cells,rule ) {
    animate( context, cells, rule, 1, 0 );
}


function animate( context,cells,rule,t,delay ) {
    console.log( "Drawing iter: " + t );
    drawCells( context, cells, t );
    if( t !== cells.T ) {
        update(t,cells,rule);
        setTimeout( animate, delay, context, cells, rule, t+1, delay );
    }
}



